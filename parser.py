from lexer import Lexer

__all__ = ['Lexer', 'Parser']

__BNF_LEXER__ = Lexer('__BNF_GRAMMAR__', **{
    'IDENTIFIER': r"([a-zA-Z][a-zA-Z0-9_]*)(:[a-zA-Z][a-zA-Z0-9_]+)?",
    'ASSIGNER': r'\=',
    'LITERAL': r'\".+?\"',
    'SPLITER': r'\|',
    'EOL': r'\;',
    '_ignore': r'[ \t]+',
    '_newline': r'\n+',
})


class table(object):
    def __init__(self):
        self.__ROWS__ = set()
        self.__COLS__ = set()
        self.__DATA__ = dict()

    def __getitem__(self, item):
        assert isinstance(item, tuple) and len(item) == 2
        assert isinstance(item[0], str) and isinstance(item[1], str)
        col = self.__DATA__.get(item[1])
        return col.get(item[0]) if col is not None else None

    def __setitem__(self, item, value):
        assert isinstance(item, tuple) and len(item) == 2
        assert isinstance(item[0], str) and isinstance(item[1], str)
        if item[1] not in self.__COLS__:
            self.__DATA__[item[1]] = dict()
            self.__COLS__.add(item[1])
        self.__DATA__[item[1]][item[0]] = value
        self.__ROWS__.add(item[0])

    @property
    def rows(self):
        return self.__ROWS__

    @property
    def cols(self):
        return self.__COLS__


class Stack:

    def __init__(self):
        self.__buffer__ = list()

    def push(self, _item):
        self.__buffer__.append(_item)

    def pop(self, _count: int = 1) -> list:
        result = self.__buffer__[- _count:]
        self.__buffer__ = self.__buffer__[:-_count]
        return result

    @property
    def top(self):
        return self.__buffer__[-1]


class Rule(object):

    def __init__(self, name: str, target: str, items: list[str], action: callable = None):
        self.name = name
        self.target = target
        self.items: tuple = tuple(items)
        self.ACTION = action

    def __getitem__(self, item):
        return self.items[item]

    def __len__(self):
        return len(self.items)

    def __eq__(self, other):
        return self.__class__ == other.__class__ and hash(self) == hash(other)

    def __hash__(self):
        return hash((self.target, self.name, self.items))

    def __repr__(self):
        return f"{self.target}.{self.name}"

    def __str__(self):
        return f"{self.target} -> {' '.join(self.items)}"

    def __call__(self, products):
        if self.ACTION is None:
            return products
        return self.ACTION(products)


class LrItem(object):
    def __init__(self, rule: Rule, lookahead: str, pos: int):
        self.rule = rule
        self.lookahead = lookahead
        self.pos = pos

    def __eq__(self, other):
        return self.__class__ == other.__class__ and hash(self) == hash(other)

    def __hash__(self):
        return hash((self.rule, self.lookahead, self.pos))

    @property
    def is_end(self):
        return self.pos >= len(self.rule)

    @property
    def current(self):
        return self.rule[self.pos] if self.pos < len(self.rule) else None

    @property
    def ahead(self):
        if self.pos + 1 >= len(self.rule):
            return None
        return self.rule[self.pos + 1]

    def __repr__(self):
        # return f"({self.rule.name}, {self.pos}, {self.lookahead})"
        return f"{self}"

    def __next__(self):
        if self.pos + 1 > len(self.rule):
            return None
        return LrItem(self.rule, self.lookahead, self.pos + 1)

    def __str__(self):
        return self.rule.target + ' -> ' \
            + ' '.join(self.rule.items[:self.pos]) + ' * ' \
            + ' '.join(self.rule.items[self.pos:]) + f' [{self.lookahead}, {self.rule.name}]'

    def __and__(self, other):
        return self.rule == other.rule and self.pos == other.pos


def in_core(items1: set[LrItem], items2: set[LrItem]) -> bool:
    if len(items1 - items2): return False
    for item in items1:
        if not any({item & i for i in items1}):
            return False
    return True


class Parser(object):

    def __init__(self, name: str, **kwargs):
        super().__init__()
        self.__RULES__ = dict()
        self.__TOKENS__ = set()
        self.__TARGETS__ = dict()
        self.__TABLE__ = None
        self.__LEXER__ = kwargs.get('lexer') or None
        self.lineno = kwargs.get("lineno") or 0
        self.colpos = kwargs.get("colpos") or 0
        self._error = None
        self.name = name

    @property
    def tokens(self):
        return self.__TOKENS__

    @property
    def rules(self):
        return set(self.__RULES__.keys())

    @property
    def targets(self):
        return set(self.__TARGETS__)

    def dump_table(self, output=None):
        file = open(output or f'{self.name}.csv', 'w')
        file.write('STATUS' + ',')
        for col in sorted(self.__TABLE__.cols):
            file.write(col + ',')
        file.write('\n')
        for row in sorted(self.__TABLE__.rows, reverse=False, key=lambda x: int(x)):
            file.write(row + ',')
            for col in sorted(self.__TABLE__.cols):
                file.write(str(self.__TABLE__[row, col] or '') + ',')
            file.write('\n')
        file.close()

    def __call__(self, __func: callable):
        if __func.__name__ == '_error':
            self._error = __func
            return __func
        self.__parser_rule__(__func.__doc__, __func.__name__, __func)
        return __func

    def __first_set__(self, _item: str):
        _first_set = set()
        if _item in self.targets:
            for rule in self.__TARGETS__[_item].values():
                if len(rule) == 0:
                    continue
                elif rule[0] != _item:
                    _first_set |= self.__first_set__(rule[0])
        else:
            _first_set.add(_item)
        return _first_set

    def __follow_set__(self, _item: str):
        _follow_set = set()
        rules = {rule for rule in self.__RULES__.values() if _item in rule}
        for rule in rules:
            indies = [i for i in range(len(rule)) if rule[i] == _item]
            for index in indies:
                if index == len(rule) - 1:
                    if rule.target == _item: continue
                    _follow_set |= self.__follow_set__(rule.target)
                else:
                    _follow_set |= self.__first_set__(rule[index + 1])
        return _follow_set

    def __parser_rule__(self, _rule_str: str, _rule_name: str, _action: callable):
        tokens = __BNF_LEXER__.tokenize(_rule_str)
        _rule_target = next(tokens)
        assert _rule_target.type == 'IDENTIFIER'
        if _rule_target.value not in self.targets:
            self.__TARGETS__.update({_rule_target.value: dict()})
            self.__TOKENS__ -= {_rule_target.value}
        token = next(tokens)
        assert token.type == 'ASSIGNER'
        _rules = dict()

        items = []
        for t in tokens:
            if t.type == 'SPLITER':
                name = f"{_rule_name}.{len(_rules)}"
                _rules[name] = Rule(name, _rule_target.value, items, _action)
                items = []
                continue
            if t.value not in self.targets:
                self.__TOKENS__.add(t.value)
            items.append(t.value)
        name = f"{_rule_name}.{len(_rules)}"
        _rules[name] = Rule(name, _rule_target.value, items, _action)
        self.__TARGETS__[_rule_target.value].update(_rules)
        self.__RULES__.update(_rules)

    @classmethod
    def __group__(cls, _items: set[LrItem]):
        ways = dict()
        for item in _items:
            way = ways.get(item.current)
            way = way or set()
            way.add(item)
            ways.update({item.current: way})
        return ways

    def __item_closure__(self, _items: set[LrItem]):
        if not _items: return set()
        current = set()
        for lr_item in _items:
            if lr_item.is_end: continue
            lookaheads = {lr_item.lookahead}
            if lr_item.ahead is not None:
                lookaheads = self.__first_set__(lr_item.ahead) or lookaheads
            if lr_item.current in self.targets:
                for r in self.__TARGETS__[lr_item.current].values():
                    current.update(LrItem(r, l, 0) for l in lookaheads)
        return _items | current | self.__item_closure__(current - _items)

    def build(self, target: str, output=None):
        ext_target = "__START__"
        ext_rule = Rule("__START__", ext_target, [target])
        start_items = self.__item_closure__({LrItem(ext_rule, '$', 0)})
        _status_items, _table_ = [start_items], table()
        out_file = open(output or f'{self.name}.lalr', 'w')
        for i, status in enumerate(_status_items):
            ways = self.__group__(status)
            for w in ways:
                if w is None:
                    for item in ways[w]:
                        _table_[str(i), item.lookahead] = f'R {item.rule.name}'
                else:
                    items = {next(i) for i in ways[w]}
                    items = self.__item_closure__(items)
                    for j in range(len(_status_items)):
                        if in_core(items, _status_items[j]): break
                    else:
                        _status_items.append(items)
                        j = len(_status_items) - 1
                    _table_[str(i), w] = f"G{j}" if w in self.targets else f"S{j}"
            out_file.write(f"----------STATUS[{i}]----{len(status)} LR Items----\n\n")
            for item in status: out_file.write(str(item) + '\n')
            out_file.write('\n' * 4)
        out_file.close()
        self.__TABLE__ = _table_

    def bind_lexer(self, lexer: Lexer) -> None:
        self.__LEXER__ = lexer

    def parse(self, _input: str):
        assert self.__TABLE__ is not None, f"Parser is not built yet!"
        status_stack = Stack()
        status_stack.push('0')
        param_stack = Stack()
        _tokens = self.__LEXER__.tokenize(_input, token=None, lineno=self.lineno, colpos=self.colpos)
        temp, _token = None, next(_tokens, None)
        while True:
            action = self.__TABLE__[status_stack.top, _token.type if _token else '$']
            if action and action[0] == 'S':
                param_stack.push(_token)
                status = action.strip('S')
                status_stack.push(status)
                _token = next(_tokens, None)
            elif action and action[0] == 'R':
                rule_name = action.strip('R ')
                if rule_name == '__START__':
                    break
                rule = self.__RULES__[action.strip('R ')]
                params = param_stack.pop(len(rule))
                status_stack.pop(len(rule))
                param_stack.push(rule.ACTION(params))
            elif self.__TABLE__[status_stack.top, param_stack.top.type]:
                goto = self.__TABLE__[status_stack.top, param_stack.top.type]
                status = goto.strip('G')
                status_stack.push(status)
            else:
                status = status_stack.top
                if self._error is None:
                    raise SyntaxError(f'Unexpected token "{_token}" at status "{status}".')
                self._error(status, _token)
                break
        return param_stack.top
