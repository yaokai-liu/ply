import re

__all__ = ['Token', 'Lexer']


class Token(object):

    def __new__(cls, _type, _value, _lineno, _colpos):
        if not _value:
            return None
        _inst = object.__new__(cls)
        return _inst

    def __init__(self, _type, _value, _lineno, _colpos):
        self.type = _type
        self.value = _value if isinstance(_value, str) else _value.group(0)
        self.lineno = _lineno
        self.colpos = _colpos
        self.matched = None if isinstance(_value, str) else _value

    def __repr__(self):
        return f'Token({self.type}, "{self.value}", {self.lineno}, {self.colpos})'

    def __len__(self):
        return len(self.value)


class Lexer(object):

    def __init__(self, name: str, lineno=0, colpos=0, **kwargs):
        super().__init__()
        self.__TOKENS__ = dict()
        self.__ASSISTS__ = dict()
        self.__LITERALS__: list = []
        self.__PRIORITIES__ = dict()
        self.name = name
        self.lineno = lineno
        self.colpos = colpos
        if kwargs:
            self.add_patterns(**kwargs)

    @property
    def tokens(self) -> list:
        return list(self.__TOKENS__.keys())

    @property
    def assists(self) -> list:
        return list(self.__ASSISTS__.keys())

    @property
    def literal_patterns(self):
        literals = [re.escape(l) for l in self.__LITERALS__]
        return re.compile(r'|'.join(literals))

    def __check_token__(self, _token):
        return _token in self.tokens

    def __check_literal__(self, _literal):
        return _literal in self.__LITERALS__

    def __compile_token__(self, _name, _token):
        if callable(_token):
            return _token

        def __match(_input):
            pattern = re.compile(_token)
            result = pattern.match(_input)
            return Token(_name, result, self.lineno, self.colpos)

        return __match

    def __compile_assist__(self, _name, _assist):
        if callable(_assist):
            return _assist

        def __assist(_input):
            pattern = re.compile(_assist)
            result = pattern.match(_input)
            return result.group(0) if result else ''

        return __assist

    def add_literals(self, literals):
        self.__LITERALS__ += literals

    def add_patterns(self, priority=0, **kwargs):
        pri_keys = self.__PRIORITIES__.get(priority)
        pri_keys = pri_keys or []
        for key, value in kwargs.items():
            if key == '_error':
                raise SyntaxError("key '_error' is not allowed set as a pattern!")
            if key.startswith('_'):
                self.__ASSISTS__.update({key: self.__compile_assist__(key, value)})
            elif key == 'literals':
                self.add_literals(value)
            else:
                self.__TOKENS__.update({key: self.__compile_token__(key, value)})
                pri_keys.append(key)
        self.__PRIORITIES__.update({priority:pri_keys})

    def __call__(self, priority=0):
        def __decorator(__func):
            name = __func.__name__
            if name == '_error':
                self.__ASSISTS__[name] = __func
                return __func
            target = self.__ASSISTS__ if name.startswith('_') else self.__TOKENS__
            _compiler = Lexer.__compile_assist__ if name.startswith('_') else Lexer.__compile_token__

            def __wrapper(_input):
                __match = _compiler(self, name, __func.__doc__)
                return __func(__match(_input))

            target[name] = __wrapper
            if not name.startswith('_'):
                p = priority or 1
                if not self.__PRIORITIES__.get(p):
                    self.__PRIORITIES__[p] = [name]
                else:
                    self.__PRIORITIES__[p].append(name)
            return __func

        return __decorator

    def __pass_space(self, _input, lineno, colpos):
        ignore = self.__ASSISTS__.get('_ignore') or (lambda x: '')
        newline = self.__ASSISTS__.get('_newline') or (lambda x: '')
        _i, _n = len(ignore(_input)), len(newline(_input))
        while _i or _n:
            if _i:
                _input = _input[_i:]
                colpos += _i
            if _n:
                _input = _input[_n:]
                lineno, colpos = lineno + 1, 0
            _i, _n = len(ignore(_input)), len(newline(_input))

        return _input, lineno, colpos

    def __error__(self, _input, lineno, colpos):
        if self.__ASSISTS__.get('_error'):
            self.__ASSISTS__['_error'](_input, lineno, colpos)
        else:
            raise SyntaxError(f"""<{self.name}> Unrecognized symbol "{_input[0]}" at line {lineno} col {colpos}.""")

    def __match(self, _input, **kwargs):
        self.lineno = kwargs.get('lineno') or 0
        self.colpos = kwargs.get('colpos') or 0
        if kwargs.get('mode') == 'longest':
            result, _type = None, None
            for p in sorted(self.__PRIORITIES__.keys(), reverse=True):
                for key in self.__PRIORITIES__[p]:
                    _res = self.__TOKENS__[key](_input)
                    if _res.span()[1] > result.span()[0]:
                        result, _type = _res, key
            if result:
                return result
            elif self.literal_patterns.match(_input):
                res = self.literal_patterns.match(_input)
                return Token(f'"{res.group(0)}"', res, self.lineno, self.colpos)
            else:
                return None
        # elif kwargs.get('mode') == 'fastest':
        else:
            for p in sorted(self.__PRIORITIES__.keys(), reverse=True):
                for key in self.__PRIORITIES__[p]:
                    result = self.__TOKENS__[key](_input)
                    if result:
                        return result
            if self.literal_patterns.match(_input):
                res = self.literal_patterns.match(_input)
                return Token(f'"{res.group(0)}"', res, self.lineno, self.colpos)
            return None

    def lex(self, _input, token=None, **kwargs):
        if not token:
            return self.__match(_input)
        self.lineno = kwargs.get('lineno') or 0
        self.colpos = kwargs.get('colpos') or 0
        if token == '_LITERAL_':
            if self.literal_patterns.match(_input):
                res = self.literal_patterns.match(_input)
                return Token(f'"{res.group(0)}"', res, self.lineno, self.colpos)
            else:
                return None
        return self.__TOKENS__[token](_input)

    def tokenize(self, _input, **kwargs):
        lineno = kwargs.get('lineno') or 0
        colpos = kwargs.get('colpos') or 0
        _input, lineno, colpos = self.__pass_space(_input, lineno, colpos)
        while _input:
            kwargs.update({'lineno': lineno, 'colpos': colpos})
            token = self.__match(_input, **kwargs)
            if not token:
                self.__error__(_input, lineno, colpos)
                return None
            yield token
            lineno = lineno
            colpos = colpos + len(token.value)
            _input = _input[len(token.value):]
            _input, lineno, colpos = self.__pass_space(_input, lineno, colpos)


if __name__ == '__main__':
    rule = Lexer('rule')
    rule.add_literals(['{','}', '(', ')', ';'])
    rule.add_patterns(TOKEN=r"\btoken\b", KEYWORD=r'\bint\b')


    @rule(1)
    def IDENTIFIER(src):
        r"""\b[a-zA-Z_][a-zA-Z0-9_]*\b"""
        return src


    @rule(1)
    def _ignore(src):
        r"""[ \t]+"""
        return src


    @rule(1)
    def _newline(src):
        r"""\n+"""
        return src


    @rule(1)
    def _error(src, lineno, colpos):
        raise SyntaxError(f"Unrecognized symbol '{src[0]}' at line {lineno} col {colpos}.")


    data = "token int (){}; apsdofunqoiuefbalsiduf"
    tokens = rule.tokenize(data)
    for t in tokens:
        print(t)
