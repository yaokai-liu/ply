if __name__ == '__main__':
    from parser import *

    demo = Parser('Top', lineno=0, colpos=0)


    @demo
    def rule_R(p):
        """
        R = L
        """

        class R:
            def __init__(self, pa):
                self.type = 'R'
                self.value = pa

        return R(p)


    @demo
    def rule_L(p):
        """
        L = "*" R | id
        """

        class L:
            def __init__(self, pa):
                self.type = 'L'
                self.value = pa

        return L(p)


    @demo
    def rule_S(p):
        """
        S = L "=" R | R
        """

        class S:
            def __init__(self, pa):
                self.type = 'S'
                self.value = pa

        return S(p)


    demo.build('S')
    demo.dump_table()
    lexer = Lexer('Top', id=r'\b[a-zA-Z_][a-zA-Z0-9_]*\b', literals=['*', '='], _ignore=' ', _newline='\n')
    demo.bind_lexer(lexer)
    r = demo.parse("* ab = * * * ac")
    print(r)
